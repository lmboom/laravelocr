<?php

namespace Lmboom\laravelOcr\Facades;

use Illuminate\Support\Facades\Facade;

class Ocr extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ocr';
    }
}
