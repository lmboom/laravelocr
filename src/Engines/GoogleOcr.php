<?php

namespace Lmboom\laravelOcr\Engines;

use Google\Cloud\Vision\VisionClient;
use Google\Cloud\Vision\Image;
use Lmboom\laravelOcr\Exceptions\GoogleVisionNoImageException;

final class GoogleOcr extends VisionClient implements Ocr
{
    private $image;
    private $options = ['text'];


    public function scan() : string
    {
        if (!key_exists('api_key', $this->options)) {
            $this->options['key'] = config('ocr.google_vision.api_key');
        }
        if (!$this->image) {
            throw new GoogleVisionNoImageException('No photo to scan');
        }

        return parent::annotate($this->image)->text()[0];
    }

    public function setImage($image)
    {
        $image = new Image($image, $this->options);

        $this->image = $image;

        return $this;
    }


}
