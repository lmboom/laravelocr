<?php

namespace Lmboom\laravelOcr\Engines;

use Illuminate\Http\File;
use Lmboom\laravelOcr\Exceptions\TesseractNoImageAvailableException;
use Lmboom\laravelOcr\Services\TesseractShell;


/**
 * @property File|string image
 */
class TesseractOcr implements Ocr
{
    private $image_path;
    private $lang = 'eng';
    private $shell;

    /**
     * TesseractVision constructor.
     * @param TesseractShell $shell
     */
    public function __construct(TesseractShell $shell)
    {
        $this->shell = $shell;
    }

    /**
     * @return string
     * @throws TesseractNoImageAvailableException
     * @throws \Lmboom\laravelOcr\Exceptions\TesseractScanFailedException
     */
    public function scan() : string
    {
        if (!$this->image) {
            throw new TesseractNoImageAvailableException('No photo to scan');
        }
        $command = $this->generateCommand();
        $result = $this->shell->execute($command);

        return $result;
    }

    /**
     * @param $image string|File
     * @return $this
     */
    public function setImage($image)
    {
        if ($image instanceof File) {
            $image = $image->getRealPath();
        }
        $this->image_path = $image;

        return $this;
    }

    public function setLanguage($language)
    {
        $this->lang = $language;

        return $this;
    }

    /**
     *Generate Tesseract shell command
     * @return string
     */
    protected function generateCommand() : string
    {
        return trim("$this->image_path stdout -l $this->lang");
    }

}
