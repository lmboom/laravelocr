<?php

namespace Lmboom\laravelOcr\Engines;

interface Ocr
{
    public function scan() : string;
}
