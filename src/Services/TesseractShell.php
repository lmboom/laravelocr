<?php

namespace Lmboom\laravelOcr\Services;


use Lmboom\laravelOcr\Exceptions\TesseractScanFailedException;
use Symfony\Component\Process\Process;

final class TesseractShell
{
    public $executable_command = 'tesseract';

    private $shellCommand;

    public function __construct(Process $shellCommand)
    {
        $this->shellCommand = $shellCommand;
    }

    /**
     * Execute Shell command to run Tesseract
     * @param string $command
     * @return string
     * @throws TesseractScanFailedException
     */
    public function execute(string $command) : string
    {
        $arguments = $this->getCommandArguments($command);

        $process = $this->shellCommand->setCommandLine($arguments);

        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new TesseractScanFailedException($process->getErrorOutput());
        }

        return $process->getOutput();
    }

    /**
     * @param string $arguments
     * @return array
     */
    protected function getCommandArguments(string $arguments) : array
    {
        return explode("$this->executable_command $arguments", ' ');
    }
}
