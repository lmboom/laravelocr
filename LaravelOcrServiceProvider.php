<?php

namespace Lmboom\laravelOcr;

use Illuminate\Support\ServiceProvider;
use Lmboom\laravelOcr\Engines\GoogleOcr;
use Lmboom\laravelOcr\Engines\TesseractOcr;
use Lmboom\laravelOcr\Engines\Ocr;

class LaravelOcrServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /* Register our commands */
        $this->registerArtisanCommands();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__ . '/../config/ocr.php' => config_path('ocr.php'),
        ]);

        $this->bindEnginesClasses();
        $this->resolvedEngineClass();

    }

    private function resolvedEngineClass()
    {
        $ocrEngine = config('ocr.ocr_engine');
        $ocrEngine = $this->app->make("$ocrEngine");

        $this->app->singleton(Ocr::class, function() use ($ocrEngine) {return $ocrEngine;});
        $this->app->singleton('ocr',  function() use ($ocrEngine) {return $ocrEngine;});
    }

    private function bindEnginesClasses()
    {
        $this->app->bind('tesseract',TesseractOcr::class);
        $this->app->bind('google_vision',GoogleOcr::class);
    }

    private function registerArtisanCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ImageParsing::class,
            ]);
        }
    }

}
