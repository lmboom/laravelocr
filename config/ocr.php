<?php

return [

    /*
       |--------------------------------------------------------------------------
       | Default OCR Engine Name
       |--------------------------------------------------------------------------
       |
       | Here you may specify which of the OCR engines below you wish
       | to use as your default engine.
       |
       */


    'ocr_engine' => env('OCR_ENGINE', 'tesseract'),

    'engines' => [
        'tesseract' => [
            'lang' => env('OCR_LANG', 'eng')
        ],

        'google_vision' => [
            'google_api_key' => env('GOOGLE_API_KEY', 'mySecretGoogleKey'),
        ]
    ]

];
