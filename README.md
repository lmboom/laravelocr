# Laravel OCR
## Installation
```
composer require lmboob/laravel-ocr
```

##Config(optional)

```
//Publish your config file. There you can choose another ocr engine and their params(default is Tesseract)
php artisan vendor:publish --tag=vision

```

### Usage
```php
// Obtaining instance via Facade alias
use Ocr;

Ocr::scan('image-path')

// Or, you can obtain Ocr instance directly
use Lmboom\LaravelOcr\Engines\Ocr;
public function method(Ocr $ocr)
```
### Scan image
```php
$imageText = $ocr->scan("image-path");

// You can also pass options
$imageText = $ocr->scan("image-path", ["lang" => "eng"]);
```

### Scan command
```php

// You can get image text via artisan command

php artisan scan:image image-path -l lang
```
